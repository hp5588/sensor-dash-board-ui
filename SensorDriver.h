#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QtCore/qvariant.h>

#define SRV_TYPE_MMA_X  0X00
#define SRV_TYPE_MMA_Y  0X01
#define SRV_TYPE_MMA_Z  0X02

#define SRV_TYPE_POT    0X03
#define SRV_TYPE_LDR    0X04
#define SRV_TYPE_NTC    0X05

#define SRV_TYPE_JS_X   0X06
#define SRV_TYPE_JS_Y   0X07
#define SRV_TYPE_JS_B   0X08

#define SRV_TYPE_US     0X09

#define SRV_TYPE_BUTTONS    0X0A


#define SRV_SYNC_0 0xFF
#define SRV_SYNC_1 0xFE
#define SRV_SYNC_2 0xFD
#define BIT0                   (0x0001)
#define BIT1                   (0x0002)
#define BIT2                   (0x0004)
#define BIT3                   (0x0008)
#define BIT4                   (0x0010)
#define BIT5                   (0x0020)
#define BIT6                   (0x0040)
#define BIT7                   (0x0080)
#define BIT8                   (0x0100)

#define BUTTON_PB1 BIT0
#define BUTTON_PB2 BIT1
#define BUTTON_PB3 BIT2
#define BUTTON_PB4 BIT3
#define BUTTON_PB5 BIT4
#define BUTTON_PB6 BIT5

#define LED_D1 BIT0
#define LED_D2 BIT1
#define LED_D3 BIT2
#define LED_D4 BIT3

// packet: SYNC*3 | CMD | len | data
#define CLN_CMD_LED             0x00
#define CLN_CMD_RELAY           0x01
#define CLN_CMD_LCD_PUT_TEXT    0x02
#define CLN_CMD_LCD_CLEAR       0x03
#define CLN_CMD_LCD_HOME        0x04



class SensorDriver : public QObject
{
    typedef enum {
        INIT,
        SYNC_0,
        SYNC_1,
        SYNC_2,
        TYPE,
        LEN,
        VALUE_0,
        VALUE_1,
        END
    } State;

    Q_OBJECT
    QSerialPort m_serial_port;
    QMessageLogger logger;
    State m_state = INIT;


    // data of current attribute
    uint8_t m_len = 0;
    uint8_t m_type = 0;
    uint8_t m_value_1 = 0;
    uint8_t m_value_0 = 0;

    // data to expose
    QVariant m_sensor_pot_val;
    QVariant m_sensor_ntc_val;
    QVariant m_sensor_ldr_val;

    QVariant m_sensor_mma_x;
    QVariant m_sensor_mma_y;
    QVariant m_sensor_mma_z;

    QVariant m_sensor_js_x;
    QVariant m_sensor_js_y;
    QVariant m_sensor_js_b;

    QVariant m_sensor_us_dist;

    QVariant m_sensor_bt_pb1;
    QVariant m_sensor_bt_pb2;
    QVariant m_sensor_bt_pb3;
    QVariant m_sensor_bt_pb4;
    QVariant m_sensor_bt_pb5;
    QVariant m_sensor_bt_pb6;

    QVariant m_sensor_led_d1;
    QVariant m_sensor_led_d2;
    QVariant m_sensor_led_d3;
    QVariant m_sensor_led_d4;
    QVariant m_sensor_lcd_text;

    QVariant m_com_port_name;
    QVariant m_is_port_opened;
    char m_led_status = 0;
    
public slots:
    void lcdClear();

private slots:
    void handleReadyRead();

private:
    void updateAttribute();
    void sendCmd(uint8_t cmd, uint8_t len, char * data);
    uint16_t uint8sToUint16(uint8_t msb, uint8_t lsb);
    void ledChangeHandler(uint8_t led, bool on);


    void openPort(QString portName);

public:
    explicit SensorDriver(QObject *parent = nullptr);

    Q_PROPERTY(QVariant potVal READ getMSensorPotVal WRITE setMSensorPotVal NOTIFY potValChanged)
    Q_PROPERTY(QVariant ntcVal READ getMSensorNtcVal WRITE setMSensorNtcVal NOTIFY ntcValChanged)
    Q_PROPERTY(QVariant ldrVal READ getMSensorLdrVal WRITE setMSensorLdrVal NOTIFY ldrValChanged)
    Q_PROPERTY(QVariant jsXVal READ getSensorJsX WRITE setSensorJsX NOTIFY jsXValChanged)
    Q_PROPERTY(QVariant jsYVal READ getSensorJsY WRITE setSensorJsY NOTIFY jsYValChanged)
    Q_PROPERTY(QVariant jsBVal READ getSensorJsB WRITE setSensorJsB NOTIFY jsBValChanged)
    Q_PROPERTY(QVariant usDistVal READ getSensorUsDist WRITE setSensorUsDist NOTIFY usDistValChanged)
    Q_PROPERTY(QVariant btPb1Val READ getSensorBtPb1 WRITE setSensorBtPb1 NOTIFY btPb1ValChanged)
    Q_PROPERTY(QVariant btPb2Val READ getSensorBtPb2 WRITE setSensorBtPb2 NOTIFY btPb2ValChanged)
    Q_PROPERTY(QVariant btPb3Val READ getSensorBtPb3 WRITE setSensorBtPb3 NOTIFY btPb3ValChanged)
    Q_PROPERTY(QVariant btPb4Val READ getSensorBtPb4 WRITE setSensorBtPb4 NOTIFY btPb4ValChanged)
    Q_PROPERTY(QVariant btPb5Val READ getSensorBtPb5 WRITE setSensorBtPb5 NOTIFY btPb5ValChanged)
    Q_PROPERTY(QVariant btPb6Val READ getSensorBtPb6 WRITE setSensorBtPb6 NOTIFY btPb6ValChanged)
    Q_PROPERTY(QVariant mmaXVal READ getSensorMmaX WRITE setSensorMmaX NOTIFY mmaXValChanged)
    Q_PROPERTY(QVariant mmaYVal READ getSensorMmaY WRITE setSensorMmaY NOTIFY mmaYValChanged)
    Q_PROPERTY(QVariant mmaZVal READ getSensorMmaZ WRITE setSensorMmaZ NOTIFY mmaZValChanged)
    Q_PROPERTY(QVariant ledD1Val READ getSensorLedD1 WRITE setSensorLedD1 NOTIFY ledD1ValChanged)
    Q_PROPERTY(QVariant ledD2Val READ getSensorLedD2 WRITE setSensorLedD2 NOTIFY ledD2ValChanged)
    Q_PROPERTY(QVariant ledD3Val READ getSensorLedD3 WRITE setSensorLedD3 NOTIFY ledD3ValChanged)
    Q_PROPERTY(QVariant ledD4Val READ getSensorLedD4 WRITE setSensorLedD4 NOTIFY ledD4ValChanged)
    Q_PROPERTY(QVariant lcdTextVal READ getSensorLcdText WRITE setSensorLcdText NOTIFY lcdTextValChanged)
    
    Q_PROPERTY(QVariant portName READ getComPortName WRITE setComPortName NOTIFY portNameValChanged)
    Q_PROPERTY(QVariant isPortOpened READ getIsPortOpened WRITE setIsPortOpened NOTIFY portOpenedValChanged)


    QVariant getMSensorPotVal() const;
    void setMSensorPotVal(QVariant mSensorPotVal);
    const QVariant &getMSensorNtcVal() const;
    void setMSensorNtcVal(const QVariant &mSensorNtcVal);
    const QVariant &getMSensorLdrVal() const;
    void setMSensorLdrVal(const QVariant &mSensorLdrVal);
    const QVariant &getSensorJsX() const;
    const QVariant &getSensorJsY() const;
    const QVariant &getSensorJsB() const;
    const QVariant &getSensorUsDist() const;
    const QVariant &getSensorBtPb1() const;
    const QVariant &getSensorBtPb2() const;
    const QVariant &getSensorBtPb3() const;
    const QVariant &getSensorBtPb4() const;
    const QVariant &getSensorBtPb5() const;
    const QVariant &getSensorBtPb6() const;
    const QVariant &getSensorMmaX() const;
    const QVariant &getSensorMmaY() const;
    const QVariant &getSensorMmaZ() const;

    const QVariant &getSensorLedD1() const;
    const QVariant &getSensorLedD2() const;
    const QVariant &getSensorLedD3() const;
    const QVariant &getSensorLedD4() const;
    const QVariant &getSensorLcdText() const;

    const QVariant &getComPortName() const;
    const QVariant &getIsPortOpened() const;




    void setSensorJsX(const QVariant &jsX);
    void setSensorJsY(const QVariant &jsY);
    void setSensorJsB(const QVariant &jsB);
    void setSensorUsDist(const QVariant &usDist);
    void setSensorBtPb1(const QVariant &btPb1);
    void setSensorBtPb2(const QVariant &btPb2);
    void setSensorBtPb3(const QVariant &btPb3);
    void setSensorBtPb4(const QVariant &btPb4);
    void setSensorBtPb5(const QVariant &btPb5);
    void setSensorBtPb6(const QVariant &btPb6);
    void setSensorMmaX(const QVariant &mmaX);
    void setSensorMmaY(const QVariant &mmaY);
    void setSensorMmaZ(const QVariant &mmaZ);

    void setSensorLedD1(const QVariant &ledD1);
    void setSensorLedD2(const QVariant &ledD2);
    void setSensorLedD3(const QVariant &ledD3);
    void setSensorLedD4(const QVariant &ledD4);
    void setSensorLcdText(const QVariant &lcdText);

    void setComPortName(const QVariant &portName);
    void setIsPortOpened(const QVariant &portOpened);

signals:
    void potValChanged();
    void ntcValChanged();
    void ldrValChanged();
    void jsXValChanged();
    void jsYValChanged();
    void jsBValChanged();
    void usDistValChanged();
    void btPb1ValChanged();
    void btPb2ValChanged();
    void btPb3ValChanged();
    void btPb4ValChanged();
    void btPb5ValChanged();
    void btPb6ValChanged();
    void mmaXValChanged();
    void mmaYValChanged();
    void mmaZValChanged();
    void ledD1ValChanged();
    void ledD2ValChanged();
    void ledD3ValChanged();
    void ledD4ValChanged();
    void lcdTextValChanged();

    void portNameValChanged();
    void portOpenedValChanged();

};

#endif // CLIENT_H
