import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Item {
    id: root
    implicitHeight: 80
    implicitWidth: 180
    property alias title: titleText.text
    property var value
    property var min:0
    property var max:1024
    property var type: Element.GraphType.LEVEL
    property var unit: ""

    signal buttonClicked()

    property var bgColor:"#0AFF0000"
    enum GraphType{
        LEVEL,
        BUTTON,
        LED
    }

    onTypeChanged: {
        if (Component.Ready){
        switch (root.type){
            case Element.GraphType.LEVEL:
                loader.sourceComponent = levelIndicatorComponent;
                break;
            case Element.GraphType.BUTTON:
                loader.sourceComponent = buttonComponent;
                break;
            case Element.GraphType.LED:
                loader.sourceComponent = ledComponent;
                break;
        }
        }
    }


    RowLayout{
        anchors.fill: parent
        Loader{
            id: loader
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: 50
            sourceComponent: root.type === Element.GraphType.LEVEL? levelIndicatorComponent: buttonComponent
        }


        Component{
            id: levelIndicatorComponent
            Item {
                Rectangle{
                    id: levelIndicator
                    height: parent.height
                    width: 20
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: "#581845"
                    Rectangle{
                        height: (value <min) ? 0: (value > max) ? parent.height : parent.height * (value / (max-min))
                        width: parent.width
                        anchors.bottom: parent.bottom
                        color: "#C70039"
                        Behavior on height{
                            NumberAnimation {
                                property: "height"
                                duration: 200
                                easing.type: Easing.InOutQuad
                            }
                        }
                    }
                }
            }

        }

        Component{
            id:buttonComponent
            Item {
                Rectangle{
                    id: lightIndicator
                    width: 20
                    height: 20
                    radius: width/2
                    anchors.centerIn: parent
                    color: root.value > 0 ? "#ffff00" : "#af9c2d"
                }
            }

        }


        Component{
            id: ledComponent
            Item {
                STDButton{
                    anchors.centerIn: parent
                    text: "Toggle"
                    onClicked: {
                        buttonClicked()
                    }
                }
            }

        }




        Item {
            id: singleGrid
            Layout.fillWidth: true
            Layout.preferredHeight: textColumn.height

            Column{
                id: textColumn
                height: titleText.height + valueText.height
                width: Math.max(titleText.width, valueText.width)
                anchors.horizontalCenter: parent.horizontalCenter
                STDText {
                    id: titleText
                    font.family: "Tahoma"
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 18
                    font.bold: true
                    color: "#5BE9E9"
                }
                STDText {
                    id: valueText
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 18
                    text: value !== undefined ? value + " " + unit: ""
                }
            }
        }
    }



    Rectangle{
        id: bg
        color: "#0AFF0000"
        anchors.fill: parent
        z: -1
    }






}
