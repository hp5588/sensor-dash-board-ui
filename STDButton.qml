import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    id: root
    implicitWidth: 40
    implicitHeight: 30
    property alias text: labelText.text

    signal clicked();

    property var inactiveColor:"#28c3d4"
    property var activeColor:"#248ea9"

    Rectangle{
        id: bg
        anchors.fill: parent
        color: inactiveColor
    }
    STDText {
        id: labelText
        anchors.centerIn: parent
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            // signal
            root.clicked();

        }
        onPressed: {
            bg.color = activeColor
        }
        onReleased: {
            bg.color = inactiveColor
        }
    }


}
