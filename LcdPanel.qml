import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Item {
    id: root
    implicitWidth: 380
    implicitHeight: 80
    property alias text: inputTextfield.text

    signal setClicked()
    signal clearClicked()

    Control{
        anchors.fill: parent
        leftPadding: 20
        rightPadding: 20

        contentItem:
            RowLayout{
                spacing: 10
                STDText{
                    text: "LCD"
                }

                TextField{
                    id: inputTextfield
                    Layout.fillWidth: true
                }
                STDButton{
                    id: sendButton
                    text: "Set"
                    onClicked: {
                        setClicked()
                    }
                }
                STDButton{
                    id: clearButton
                    text: "Clear"
                    onClicked: {
                        clearClicked()
                    }
                }

            }
    }



    Rectangle{
        id: bg
        color: "#0AFF0000"
        anchors.fill: parent
        z: -1
    }



}
