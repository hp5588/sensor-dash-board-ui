#include "SensorDriver.h"
#include <QDebug>
#include <iostream>
SensorDriver::SensorDriver(QObject *parent) : QObject(parent)
{

    // list all serial port
    const auto ports =  QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &port : ports){
        QMessageLogger().info() << port.portName() ;
    }

    connect(&this->m_serial_port, &QSerialPort::readyRead, this, &SensorDriver::handleReadyRead);
    connect(this, &SensorDriver::portNameValChanged, [=](){this->openPort(this->m_com_port_name.toString());});

}

void SensorDriver::sendCmd(uint8_t cmd, uint8_t len, char *data) {
    uint8_t syncs[] = {SRV_SYNC_0, SRV_SYNC_1, SRV_SYNC_2};
    m_serial_port.write(reinterpret_cast<const char *>(syncs), sizeof(syncs));
    m_serial_port.write(reinterpret_cast<const char *>(&cmd), 1);
    m_serial_port.write(reinterpret_cast<const char *>(&len), 1);
    if (len > 0)
        m_serial_port.write(data, len);
}

void SensorDriver::lcdClear() {
    this->sendCmd(CLN_CMD_LCD_CLEAR, 0, nullptr);
}

void SensorDriver::handleReadyRead() {
    // logger.info("data received");
//    logger.info(data.toHex(':').toStdString().c_str());
//    logger.info(data.toStdString().c_str());
    QByteArray data = m_serial_port.readAll();


    // decode
    for (int i = 0; i < data.size(); ++i) {
        uint8_t byte = data.at(i);
        switch (m_state){
            case INIT:{
                m_state = byte == SRV_SYNC_0 ? SYNC_0: INIT;
                break;
            }
            case SYNC_0:{
                m_state = byte == SRV_SYNC_1 ? SYNC_1: INIT;
                break;
            }
            case SYNC_1:{
                m_state = byte == SRV_SYNC_2 ? TYPE: INIT;
                break;
            }
            case TYPE:
                m_type = byte;
                m_state = LEN;
                break;
            case LEN:
                this-> m_len = byte;
                m_state = VALUE_1;
                break;
            case VALUE_1:
                m_value_1 = byte;
                if (this->m_len == 2){
                    m_state = VALUE_0;
                } else{
                    m_state = END;
                }
                break;
            case VALUE_0:
                m_value_0 = byte;
                m_state = END;
                break;
            case END:
                if (byte == SRV_SYNC_0){
                    // a new session
                    m_state = SYNC_1;
                } else{
                    // next attribute
                    // TODO check if byte belongs to any of the group
                    m_type = byte;
                    m_state = LEN;
                }
                break;
        }
        if (m_state == END){
            updateAttribute();
        }
    }


}

void SensorDriver::updateAttribute(){
    switch (m_type) {
        case SRV_TYPE_MMA_X :
            setSensorMmaX(QVariant(uint8sToUint16(m_value_1, m_value_0)));
            break;
        case SRV_TYPE_MMA_Y :
            setSensorMmaY(QVariant(uint8sToUint16(m_value_1, m_value_0)));
            break;
        case SRV_TYPE_MMA_Z :
            setSensorMmaZ(QVariant(uint8sToUint16(m_value_1, m_value_0)));
            break;

        case SRV_TYPE_POT:
            setMSensorPotVal(QVariant(uint8sToUint16(m_value_1, m_value_0)));
            break;
        case SRV_TYPE_LDR:
            setMSensorLdrVal(QVariant(uint8sToUint16(m_value_1, m_value_0)));
            break;
        case SRV_TYPE_NTC:
            setMSensorNtcVal(QVariant(uint8sToUint16(m_value_1, m_value_0)));
            break;

        case SRV_TYPE_JS_X:
            setSensorJsX(QVariant(m_value_1));
            break;
        case SRV_TYPE_JS_Y:
            setSensorJsY(QVariant(m_value_1));
            break;
        case SRV_TYPE_JS_B:
            setSensorJsB(QVariant(m_value_1));
            break;
        case SRV_TYPE_US:
            setSensorUsDist(QVariant(m_value_1));
            break;

        case SRV_TYPE_BUTTONS:
            setSensorBtPb1(QVariant((m_value_1&BUTTON_PB1)>0));
            setSensorBtPb2(QVariant((m_value_1&BUTTON_PB2)>0));
            setSensorBtPb3(QVariant((m_value_1&BUTTON_PB3)>0));
            setSensorBtPb4(QVariant((m_value_1&BUTTON_PB4)>0));
            setSensorBtPb5(QVariant((m_value_1&BUTTON_PB5)>0));
            setSensorBtPb6(QVariant((m_value_1&BUTTON_PB6)>0));
            break;
    }
}



uint16_t SensorDriver::uint8sToUint16(uint8_t msb, uint8_t lsb) {
    uint16_t result = (msb << 8) + lsb;
    return  result;
}

void SensorDriver::ledChangeHandler(uint8_t led, bool on)
{
    uint8_t data[] = {on, led};
    sendCmd(CLN_CMD_LED, 2,(char *)data);
}

void SensorDriver::openPort(QString portName)
{
    if(m_serial_port.isOpen())
        m_serial_port.close();
    m_serial_port.setPortName(portName);
    m_serial_port.setBaudRate(QSerialPort::Baud9600);

     if (!m_serial_port.open(QIODevice::ReadWrite)){
             QMessageLogger().info() << QObject::tr("Failed to open port %1, error: %2")
            .arg(m_serial_port.portName()).arg(m_serial_port.error()) << endl;
            return;
     }
     setIsPortOpened(true);
     logger.info("connected");

}


QVariant SensorDriver::getMSensorPotVal() const {
    return m_sensor_pot_val;
}

void SensorDriver::setMSensorPotVal(QVariant mSensorPotVal) {
    if (mSensorPotVal != m_sensor_pot_val) {
        m_sensor_pot_val = mSensorPotVal;
        emit potValChanged();
    }
}

const QVariant &SensorDriver::getMSensorNtcVal() const {
    return m_sensor_ntc_val;
}

void SensorDriver::setMSensorNtcVal(const QVariant &mSensorNtcVal) {
    if (mSensorNtcVal != m_sensor_ntc_val) {
        m_sensor_ntc_val = mSensorNtcVal;
        emit ntcValChanged();
    }
}

const QVariant &SensorDriver::getMSensorLdrVal() const {
    return m_sensor_ldr_val;
}

void SensorDriver::setMSensorLdrVal(const QVariant &mSensorLdrVal) {
    if (mSensorLdrVal != m_sensor_ldr_val) {
        m_sensor_ldr_val = mSensorLdrVal;
        emit ldrValChanged();
    }
}



void SensorDriver::setSensorJsX(const QVariant &jsX) {
    if (jsX != m_sensor_js_x) {
        m_sensor_js_x = jsX;
        emit jsXValChanged();
    }
}
void SensorDriver::setSensorJsY(const QVariant &jsY) {
    if (jsY != m_sensor_js_y) {
        m_sensor_js_y = jsY;
        emit jsYValChanged();
    }
}
void SensorDriver::setSensorJsB(const QVariant &jsB) {
    if (jsB != m_sensor_js_b) {
        m_sensor_js_b = jsB;
        emit jsBValChanged();
    }
}
void SensorDriver::setSensorUsDist(const QVariant &usDist) {
    if (usDist != m_sensor_us_dist) {
        m_sensor_us_dist = usDist;
        emit usDistValChanged();
    }
}
void SensorDriver::setSensorBtPb1(const QVariant &btPb1) {
    if (btPb1 != m_sensor_bt_pb1) {
        m_sensor_bt_pb1 = btPb1;
        emit btPb1ValChanged();
    }
}
void SensorDriver::setSensorBtPb2(const QVariant &btPb2) {
    if (btPb2 != m_sensor_bt_pb2) {
        m_sensor_bt_pb2 = btPb2;
        emit btPb2ValChanged();
    }
}
void SensorDriver::setSensorBtPb3(const QVariant &btPb3) {
    if (btPb3 != m_sensor_bt_pb3) {
        m_sensor_bt_pb3 = btPb3;
        emit btPb3ValChanged();
    }
}
void SensorDriver::setSensorBtPb4(const QVariant &btPb4) {
    if (btPb4 != m_sensor_bt_pb4) {
        m_sensor_bt_pb4 = btPb4;
        emit btPb4ValChanged();
    }
}
void SensorDriver::setSensorBtPb5(const QVariant &btPb5) {
    if (btPb5 != m_sensor_bt_pb5) {
        m_sensor_bt_pb5 = btPb5;
        emit btPb5ValChanged();
    }
}
void SensorDriver::setSensorBtPb6(const QVariant &btPb6) {
    if (btPb6 != m_sensor_bt_pb6) {
        m_sensor_bt_pb6 = btPb6;
        emit btPb6ValChanged();
    }
}

void SensorDriver::setSensorMmaX(const QVariant &mmaX) {
    if (mmaX != m_sensor_mma_x) {
        m_sensor_mma_x = mmaX;
        emit mmaXValChanged();
    }
}
void SensorDriver::setSensorMmaY(const QVariant &mmaY) {
    if (mmaY != m_sensor_mma_y) {
        m_sensor_mma_y = mmaY;
        emit mmaYValChanged();
    }
}
void SensorDriver::setSensorMmaZ(const QVariant &mmaZ) {
    if (mmaZ != m_sensor_mma_z) {
        m_sensor_mma_z = mmaZ;
        emit mmaZValChanged();
    }
}

void SensorDriver::setSensorLedD1(const QVariant &ledD1) {
    ledChangeHandler(LED_D1, ledD1.toBool());

    if (ledD1 != m_sensor_led_d1) {
        m_sensor_led_d1 = ledD1;
        emit ledD1ValChanged();
    }
}
void SensorDriver::setSensorLedD2(const QVariant &ledD2) {
    ledChangeHandler(LED_D2, ledD2.toBool());

    if (ledD2 != m_sensor_led_d2) {
        m_sensor_led_d2 = ledD2;
        emit ledD2ValChanged();
    }
}
void SensorDriver::setSensorLedD3(const QVariant &ledD3) {
    ledChangeHandler(LED_D3, ledD3.toBool());

    if (ledD3 != m_sensor_led_d3) {
        m_sensor_led_d3 = ledD3;
        emit ledD3ValChanged();
    }
}
void SensorDriver::setSensorLedD4(const QVariant &ledD4) {
    ledChangeHandler(LED_D4, ledD4.toBool());

    if (ledD4 != m_sensor_led_d4) {
        m_sensor_led_d4 = ledD4;
        emit ledD4ValChanged();
    }
}
void SensorDriver::setSensorLcdText(const QVariant &lcdText) {
    QByteArray textArray = lcdText.toString().toUtf8();
    sendCmd(CLN_CMD_LCD_PUT_TEXT, textArray.size() , textArray.data());
    if (lcdText != m_sensor_lcd_text) {
        m_sensor_lcd_text = lcdText;
        emit lcdTextValChanged();
    }
}

void SensorDriver::setComPortName(const QVariant &portName) {
                 if (portName != m_com_port_name) {
                    m_com_port_name = portName;
                    emit portNameValChanged();
                 }
             }
void SensorDriver::setIsPortOpened(const QVariant &portOpened) {
                 if (portOpened != m_is_port_opened) {
                    m_is_port_opened = portOpened;
                    emit portOpenedValChanged();
                 }
}


const QVariant &SensorDriver::getSensorJsX() const {                return m_sensor_js_x;            }
const QVariant &SensorDriver::getSensorJsY() const {                return m_sensor_js_y;            }
const QVariant &SensorDriver::getSensorJsB() const {                return m_sensor_js_b;            }
const QVariant &SensorDriver::getSensorUsDist() const {                return m_sensor_us_dist;            }
const QVariant &SensorDriver::getSensorBtPb1() const {                return m_sensor_bt_pb1;            }
const QVariant &SensorDriver::getSensorBtPb2() const {                return m_sensor_bt_pb2;            }
const QVariant &SensorDriver::getSensorBtPb3() const {                return m_sensor_bt_pb3;            }
const QVariant &SensorDriver::getSensorBtPb4() const {                return m_sensor_bt_pb4;            }
const QVariant &SensorDriver::getSensorBtPb5() const {                return m_sensor_bt_pb5;            }
const QVariant &SensorDriver::getSensorBtPb6() const {                return m_sensor_bt_pb6;            }
const QVariant &SensorDriver::getSensorMmaX() const {                return m_sensor_mma_x;            }
const QVariant &SensorDriver::getSensorMmaY() const {                return m_sensor_mma_y;            }
const QVariant &SensorDriver::getSensorMmaZ() const {                return m_sensor_mma_z;            }
const QVariant &SensorDriver::getSensorLedD1() const {                return m_sensor_led_d1;            }
const QVariant &SensorDriver::getSensorLedD2() const {                return m_sensor_led_d2;            }
const QVariant &SensorDriver::getSensorLedD3() const {                return m_sensor_led_d3;            }
const QVariant &SensorDriver::getSensorLedD4() const {                return m_sensor_led_d4;            }
const QVariant &SensorDriver::getSensorLcdText() const {                return m_sensor_lcd_text;            }

const QVariant &SensorDriver::getComPortName() const {                return m_com_port_name;            }
const QVariant &SensorDriver::getIsPortOpened() const {                return m_is_port_opened;            }
