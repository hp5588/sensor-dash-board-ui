import QtQuick 2.0
import QtQuick.Layouts 1.3

Rectangle{
    id: splashPage
    color: "black"
    ColumnLayout{
        anchors.centerIn: parent
        spacing: 10
        Repeater{
            model: ["Sensor", "Dashboard"]
            STDText {
                text: modelData
                Layout.alignment: Qt.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 40
                font.family: "Tahoma"
            }
        }

        STDText {
            id: authorInfo
            Layout.alignment: Qt.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Powen Kao"
            font.pixelSize: 20
        }
    }


    Behavior on opacity {
        NumberAnimation {
            duration: 500
            easing.type: Easing.InOutQuad
        }
    }
}



/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
