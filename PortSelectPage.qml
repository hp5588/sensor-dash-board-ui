import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Item {
    id: root
    property alias portName: input.text
    signal clicked()
    ColumnLayout{
        width: 200
        anchors.centerIn: parent
        STDText{
            Layout.alignment: Qt.AlignHCenter
            text: "Enter A Port name"
        }
        TextField{
            id: input
            Layout.alignment: Qt.AlignHCenter
            placeholderText:"(e.g. COM5)"
        }
        STDButton{
            text: "Open"
            Layout.alignment: Qt.AlignHCenter
            onClicked: {
                root.clicked()
            }
        }
    }

}
