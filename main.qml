import QtQuick 2.12
import QtQuick.Window 2.12
import Sensor 1.0
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

Window {
    visible: true
    width: 830
    height: 640
    title: qsTr("Sensor Dashboard")
    Item {
        id: contentItem
        anchors.fill: parent
        LinearGradient{
            anchors.fill: parent
            start: Qt.point(0, 0)
            end: Qt.point(0, parent.height)
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#35b3b3" }
                GradientStop { position: 0.5; color: "#329cae" }
                GradientStop { position: 1.0; color: "#3287ae" }
            }
        }

        ColumnLayout{
            anchors.fill: parent
            Flow {
                id: flow
                Layout.preferredWidth:parent.width
                Layout.fillHeight: true
                padding: 20
                spacing: 20


                // LDR, POT, NTC, US
                Element{
                    title: "LDR"
                    value: sensor.ldrVal
                }
                Element{
                    title: "POT"
                    value: sensor.potVal
                }
                Element{
                    title: "NTC"
                    value: ntcToCelsius(sensor.ntcVal)
//                    value: sensor.ntcVal
                    unit: "℃"
                    max: 60
                }
                Element{
                    title: "Ultra Sonic"
                    value: sensor.usDistVal
                    unit: "cm"
                    max: 40
                }

                // MMA
                Element{
                    title: "MMA X"
                    max: 2048
                    value: sensor.mmaXVal
                }
                Element{
                    title: "MMA Y"
                    max: 2048
                    value: sensor.mmaYVal
                }
                Element{
                    title: "MMA Z"
                    max: 2048
                    value: sensor.mmaZVal
                }

                // JS
                Element{
                    title: "Joystick X"
                    max:255
                    value: sensor.jsXVal
                }
                Element{
                    title: "Joystick Y"
                    max:255
                    value: sensor.jsYVal
                }
                Element{
                    title: "Joystick B"
                    max: 255
                    value: sensor.jsBVal
                }

                // PB 1-6
                Element{
                    title: "Button PB1"
                    value: sensor.btPb1Val
                    type: Element.GraphType.BUTTON
                }
                Element{
                    title: "Button PB2"
                    value: sensor.btPb2Val
                    type: Element.GraphType.BUTTON
                }
                Element{
                    title: "Button PB3"
                    value: sensor.btPb3Val
                    type: Element.GraphType.BUTTON
                }
                Element{
                    title: "Button PB4"
                    value: sensor.btPb4Val
                    type: Element.GraphType.BUTTON
                }
                Element{
                    title: "Button PB5"
                    value: sensor.btPb5Val
                    type: Element.GraphType.BUTTON
                }
                Element{
                    title: "Button PB6"
                    value: sensor.btPb6Val
                    type: Element.GraphType.BUTTON
                }

                // LCD Panel
                LcdPanel{
                    id: lcdPanel
                    onSetClicked: {
                        sensor.lcdTextVal = lcdPanel.text
                    }
                    onClearClicked: {
                        sensor.lcdClear()
                    }


                }

                // LED D1-D4
                Element{
                    title: "LED D1"
                    type: Element.GraphType.LED
                    value: sensor.ledD1Val
                    onButtonClicked: {
                        sensor.ledD1Val = !sensor.ledD1Val
                    }
                }
                Element{
                    title: "LED D2"
                    type: Element.GraphType.LED
                    value: sensor.ledD2Val
                    onButtonClicked: {
                        sensor.ledD2Val = !sensor.ledD2Val
                    }
                }
                Element{
                    title: "LED D3"
                    type: Element.GraphType.LED
                    value: sensor.ledD3Val
                    onButtonClicked: {
                        sensor.ledD3Val = !sensor.ledD3Val
                    }
                }
                Element{
                    title: "LED D4"
                    type: Element.GraphType.LED
                    value: sensor.ledD4Val
                    onButtonClicked: {
                        sensor.ledD4Val = !sensor.ledD4Val
                    }
                }
            }

            Item {
                id: info
                Layout.preferredHeight: 30
                Layout.preferredWidth: parent.width
                Rectangle{
                    id: infoBg
                    anchors.fill: parent
                    color: "#805BE9E9"
                    opacity: 0
                    smooth: true
                }


                RowLayout{
                    spacing: 10
                    anchors.right: parent.right
                    anchors.rightMargin: flow.padding
                    Repeater{
                        model: ["https://gitlab.com/hp5588/sensor-dash-board-ui.git","|","Powen Kao", "|", "hp5588@gmail.com"]
                        STDText {
                            text: modelData
                            color: "#5BE9E9"
                        }
                    }
                }
            }
        }

    }

    FastBlur{
        id: blurEffect
        anchors.fill: contentItem
        source: contentItem
        radius: 32
    }


    PortSelectPage{
        anchors.fill: parent
        onClicked: {
            visible = false
            blurEffect.visible = false
            sensor.portName = portName
        }
    }

    SplashPage{
        id: splashPage
        anchors.fill: parent
        Component.onCompleted: {
            timer.start()
        }

    }


    Timer{
        id: timer
        interval: 1000
        triggeredOnStart: false
        onTriggered:{
            splashPage.opacity = 0;
        }

    }

    SensorDriver{
        id: sensor
//        portName: "COM5"
        onIsPortOpenedChanged: {
            if (isPortOpened){
                sensor.ledD1Val = true;
                sensor.ledD2Val = true;
                sensor.ledD3Val = true;
                sensor.ledD4Val = true;
            }
        }
    }

    function ntcToCelsius(rawValue){
        if (rawValue < 285 | rawValue > 380)
            return -1

        var entry =ntcTable[rawValue]
        if (!!entry){
            // exist
            return entry[0]
        }else{
            // find the closet
            var minValue = -1
            for (var key in ntcTable){
                if (key > rawValue){
                    minValue = Math.min(key, minValue)
                }else{
                    // calculate with slope
                    entry = ntcTable[minValue]
                    return entry[0] + (rawValue - minValue) / entry[1]
                }
            }

        }
    }

    property var ntcTable: [
        // raw value:[temprature, slope(temp/degree)]
        {379: [35, 5.9]},
        {373: [34, 5.7]},
        {367: [33, 5.5]},
        {362: [32, 5.4]},
        {357: [31, 5.2]},
        {351: [30, 5.1]},
        {346: [29, 4.9]},
        {341: [28, 4.8]},
        {337: [27, 4.6]},
        {332: [26, 4.5]},
        {327: [25, 4.4]},
        {323: [24, 4.3]},
        {319: [23, 4.2]},
        {315: [22, 4.1]},
        {311: [21, 4.0]},
        {307: [20, 3.9]},
        {303: [19, 3.8]},
        {299: [18, 3.7]},
        {295: [17, 3.6]},
        {292: [16, 3.5]},
        {288: [15, 3.4]},
        {285: [14, 0  ]}
    ]



}
